package core

import (
	"azure_ad/models"
	"net/http"

	log "github.com/sirupsen/logrus"
)

func RequestUsersInfo(accessToken *models.TokenRequestResponse) (resp *http.Response, err error) {

	url := "https://graph.microsoft.com/v1.0/users"
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		log.Errorf("profile info error %v", err)
		return req.Response, err
	}

	req.Header.Add("Authorization", "Bearer "+accessToken.AccessToken)

	response, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Errorf("profile Error: %v", err)
		return response, err
	}

	return response, nil
}
