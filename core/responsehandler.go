package core

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	log "github.com/sirupsen/logrus"
)

func contains(statuses []int, status int) bool {

	for _, s := range statuses {
		if status == s {
			return true
		}
	}
	return false
}

func ResponseHandler(response *http.Response) map[string]interface{} {

	var ErrorStatuses = []int{
		http.StatusNotFound,
		http.StatusServiceUnavailable,
		http.StatusBadGateway,
		http.StatusInternalServerError,
		http.StatusBadRequest,
	}

	var r = make(map[string]interface{})

	if contains(ErrorStatuses, response.StatusCode) {
		r["microservice_status"] = response.StatusCode
		r["microservice_error"] = response.Status
		r["url"] = response.Request.URL.String()
		r["message"] = "NB: This is a Microservice Services Related Error!"

		body, _ := ioutil.ReadAll(response.Body)

		defer response.Body.Close()

		var erroResp interface{}
		_ = json.Unmarshal(body, &erroResp)
		r["microservice_response"] = erroResp
		return r
	}

	body, err := ioutil.ReadAll(response.Body)

	defer response.Body.Close()
	if err != nil {
		log.Errorf(" ResponseHandler Error, %v", err.Error())
	}
	if err = json.Unmarshal(body, &r); err != nil {
		log.Errorf("Body parsing error, %v", err)
	}

	return r
}
