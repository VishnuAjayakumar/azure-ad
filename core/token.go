package core

import (
	"azure_ad/models"
	"encoding/json"
	"net/http"
	"net/url"
	"strings"

	"github.com/gofiber/fiber/v2"
)

func RequestToken(app *fiber.Ctx, authendpoint string, body url.Values) (*models.TokenRequestResponse, error) {

	var res *models.TokenRequestResponse
	request, err := http.NewRequest(http.MethodPost, authendpoint, strings.NewReader(body.Encode()))
	if err != nil {
		app.Status(fiber.StatusBadRequest).JSON(err.Error())
		return nil, err
	}

	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	client := &http.Client{}

	resp, err := client.Do(request)
	if err != nil {
		app.Status(fiber.StatusBadRequest).JSON(err.Error())
		return nil, err
	}

	json.NewDecoder(resp.Body).Decode(&res)

	return res, nil
}
