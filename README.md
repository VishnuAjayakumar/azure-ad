# Azure AD
## Getting started

config regarding generation of token for AzureAD is added in config.env file
for running this golang application to interact with AzureAD, firstly we have to run the main.go file.

go run main.go

certain endpoints are defined so that we can interact with AzureAD.

## */login* :

Endpoint which retreives token by passing username and password.We have to set the configs such as CLIENT_ID,RESOURCE,LOGIN_SCOPE,CLIENT_SECRET,LOGIN_GRANT_TYPE. 

## */users* :

Endpoint which retreives users list.We have to set the configs such as RESOURCE,CLIENT_ID,CLIENT_SECRET,GRANT_TYPE.

## Swagger 

Run main.go file, and browser to http://localhost:{port-number}/swagger, you can see Swagger 2.0 Api documents.