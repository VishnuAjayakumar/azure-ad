package handlers

import (
	"azure_ad/core"
	"net/url"
	"os"

	"github.com/gofiber/fiber/v2"
	"github.com/joho/godotenv"
	"github.com/labstack/gommon/log"
)

// GetUsers godoc
// @Summary Get users endpoint
// @Description This endpoint is for fetching users from azure.
// @Description Any http status other than 200 signifies that the application is down
// @Produce  json
// @Success 200 {object} map[string]interface{}
// @Router /users [GET]
// @Tags Authorizations
func GetUsersData(app *fiber.Ctx) error {

	//loading env variables
	err := godotenv.Load("config.env")
	if err != nil {
		log.Fatalf("Some error occured in config.env file Err: %s", err)
	}
	//configurations for azure token generation
	authendpoint := os.Getenv("TOKEN_URL")
	body := url.Values(map[string][]string{
		"resource":      {os.Getenv("RESOURCE")},
		"client_id":     {os.Getenv("CLIENT_ID")},
		"client_secret": {os.Getenv("CLIENT_SECRET")},
		"grant_type":    {os.Getenv("GRANT_TYPE")}})

	//requesting token
	accessToken, err := core.RequestToken(app, authendpoint, body)
	if err != nil {
		return app.Status(fiber.StatusBadRequest).JSON(accessToken)
	}

	//gathering users data by passing accesstoken.
	resp, err := core.RequestUsersInfo(accessToken)
	if err != nil {
		log.Error("unable to request users data : ", err)
		app.Status(fiber.StatusBadRequest)
	}

	return app.Status(fiber.StatusOK).JSON(core.ResponseHandler(resp))
}
