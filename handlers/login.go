package handlers

import (
	"azure_ad/core"
	"azure_ad/models"
	"net/url"
	"os"

	"github.com/gofiber/fiber/v2"
	"github.com/joho/godotenv"
	"github.com/labstack/gommon/log"
)

// Login godoc
// @Summary Login endpoint
// @Description This endpoint is for authenticate
// @Description Any http status other than 200 signifies that the application is down
// @Accept  json
// @Produce  json
// @Param Body body models.Login true "The body to request login"
// @Success 200 {object} models.TokenRequestResponse
// @Router /login [POST]
// @Tags Authorizations
func LoginData(app *fiber.Ctx) error {

	var messageInterface = make(map[string]interface{})

	data := models.Login{}
	if err := app.BodyParser(&data); err != nil {
		return err
	}
	//loading env variables
	err := godotenv.Load("config.env")
	if err != nil {
		log.Fatalf("Some error occured in config.env file Err: %s", err)
	}
	//configurations for azure token generation
	authendpoint := os.Getenv("TOKEN_URL")
	body := url.Values(map[string][]string{
		"client_id":     {os.Getenv("CLIENT_ID")},
		"resource":      {os.Getenv("RESOURCE")},
		"scope":         {os.Getenv("LOGIN_SCOPE")},
		"username":      {data.Username},
		"password":      {data.Password},
		"client_secret": {os.Getenv("CLIENT_SECRET")},
		"grant_type":    {os.Getenv("LOGIN_GRANT_TYPE")},
	})

	//requesting token
	accessToken, err := core.RequestToken(app, authendpoint, body)
	if err != nil {
		messageInterface["Error"] = err
		return app.Status(fiber.StatusBadRequest).JSON(messageInterface)
	}

	//checking accesstoken is nil or not
	if accessToken.AccessToken == "" {
		messageInterface["message"] = "Value of token is nil due to invalid credentials"
		return app.Status(fiber.StatusBadRequest).JSON(messageInterface)
	}
	return app.Status(fiber.StatusOK).JSON(accessToken)

}
