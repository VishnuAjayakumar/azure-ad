package main

import (
	"azure_ad/handlers"

	swagger "github.com/arsmn/fiber-swagger/v2"

	_ "azure_ad/docs"

	"github.com/gofiber/fiber/v2"
)

func setupRoutes(app *fiber.App) {

	app.Get("/swagger/*", swagger.HandlerDefault) // default
	app.Post("/login", handlers.LoginData)
	app.Get("/users", handlers.GetUsersData)

}

// @title Golang-Azure AD Api
// @version 1.0
// @description This is a simple golang application to interact with AzureAD
// @termsOfService http://swagger.io/terms/
// @contact.name API Support
// @contact.url http://www.swagger.io/support
// @contact.email support@swagger.io
// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html
// @BasePath /v2
func main() {

	app := fiber.New()

	setupRoutes(app)
	if err := app.Listen(":8080"); err != nil {
		panic(err)
	}

}
